# Autonomous Application Lab Snippets

Collection of snippets for TUM CPS "Autonomous Application Lab".

Please find the templates and examples in the folders of the same name.
